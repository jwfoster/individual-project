# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
AppointmentStatus.create([{appointment_status: 'Scheduled'},
                          {appointment_status: 'Cancelled'},
                          {appointment_status: 'Completed'}])

PhysicianStatus.create([{physician_status: 'Active'},
                        {physician_status: 'Inactive'}])

TimeSlot.create([{time_slot_string: '8:00 AM', time_slot: '08:00:00'},
                 {time_slot_string: '8:30 AM', time_slot: '08:30:00'},
                 {time_slot_string: '9:00 AM', time_slot: '09:00:00'},
                 {time_slot_string: '9:30 AM', time_slot: '09:30:00'},
                 {time_slot_string: '10:00 AM', time_slot: '10:00:00'},
                 {time_slot_string: '10:30 AM', time_slot: '10:30:00'},
                 {time_slot_string: '11:00 AM', time_slot: '11:00:00'},
                 {time_slot_string: '11:30 AM', time_slot: '11:30:00'},
                 {time_slot_string: '12:00 PM', time_slot: '12:00:00'},
                 {time_slot_string: '12:30 PM', time_slot: '12:30:00'},
                 {time_slot_string: '1:00 PM', time_slot: '13:00:00'},
                 {time_slot_string: '1:30 PM', time_slot: '13:30:00'},
                 {time_slot_string: '2:00 PM', time_slot: '14:00:00'},
                 {time_slot_string: '2:30 PM', time_slot: '14:30:00'},
                 {time_slot_string: '3:00 PM', time_slot: '15:00:00'},
                 {time_slot_string: '3:30 PM', time_slot: '15:30:00'},
                 {time_slot_string: '4:00 PM', time_slot: '16:00:00'},
                 {time_slot_string: '4:30 PM', time_slot: '16:30:00'}])

Physician.create([{physician_name: 'Julie Bowen', physician_email: 'J.Bowen@mail.com', physician_status_id: 1},
                  {physician_name: 'Ty Burrell', physician_email: 'T.Burrell@email.net', physician_status_id: 1},
                  {physician_name: 'Sarah Hyland', physician_email: 'S.Hyland@mail.com', physician_status_id: 1},
                  {physician_name: 'Eric Stonestreet', physician_email: 'E.Stonestreet@email.com', physician_status_id: 1},
                  {physician_name: 'Ariel Winter', physician_email: 'A.Winter@mail.com', physician_status_id: 1}])

open('/vagrant/DiagnosisSeedFile.csv') do |diagnose|
  diagnose.read.each_line do |diagnosis|
    d_name, d_price = diagnosis.chomp.split('|')
    Diagnosis.create!(:diagnosis_description => d_name, :diagnosis_price => d_price)
  end
end

open('/vagrant/PatientSeedFile.csv') do |patients|
  patients.read.each_line do |patient|
    name, email, address, phone = patient.chomp.split('|')
    Patient.create!(:patient_name => name, :patient_email => email, :patient_address => address, :patient_phone => phone)
  end
end