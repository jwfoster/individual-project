class CreatePhysicianStatuses < ActiveRecord::Migration
  def change
    create_table :physician_statuses do |t|
      t.string :phys_status

      t.timestamps
    end
  end
end
