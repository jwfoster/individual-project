class FixColumnName < ActiveRecord::Migration
  def self.up
    rename_column :physician_statuses, :physician_status, :phys_status
    rename_column :appointment_statuses, :appointment_status, :app_status
  end
end
