class CreateTimeSlots < ActiveRecord::Migration
  def change
    create_table :time_slots do |t|
      t.string :time_slot_string
      t.time :time_slot

      t.timestamps
    end
  end
end
