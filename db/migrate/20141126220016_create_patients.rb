class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :patient_name
      t.string :patient_email
      t.string :patient_address
      t.string :patient_phone

      t.timestamps
    end
  end
end
