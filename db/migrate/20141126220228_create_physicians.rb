class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.string :physician_name
      t.string :physician_email
      t.integer :physician_status_id

      t.timestamps
    end
  end
end
