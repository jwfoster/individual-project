class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.date :invoice_date
      t.decimal :invoice_amount
      t.integer :appointment_id

      t.timestamps
    end
  end
end
