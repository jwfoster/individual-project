class CreateAppointmentStatuses < ActiveRecord::Migration
  def change
    create_table :appointment_statuses do |t|
      t.string :app_status

      t.timestamps
    end
  end
end
