class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.integer :physician_id
      t.date :appointment_date
      t.integer :time_slot_id
      t.string :appointment_notes
      t.integer :diagnosis_id
      t.string :physician_notes
      t.integer :appointment_status_id

      t.timestamps
    end
  end
end
