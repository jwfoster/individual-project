class CreateDiagnoses < ActiveRecord::Migration
  def change
    create_table :diagnoses do |t|
      t.string :diagnosis_description
      t.decimal :diagnosis_price

      t.timestamps
    end
  end
end
