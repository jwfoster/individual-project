# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141128223702) do

  create_table "appointment_statuses", force: true do |t|
    t.string   "app_status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appointments", force: true do |t|
    t.integer  "patient_id"
    t.integer  "physician_id"
    t.date     "appointment_date"
    t.integer  "time_slot_id"
    t.string   "appointment_notes"
    t.integer  "diagnosis_id"
    t.string   "physician_notes"
    t.integer  "appointment_status_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "diagnoses", force: true do |t|
    t.string   "diagnosis_description"
    t.decimal  "diagnosis_price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoices", force: true do |t|
    t.date     "invoice_date"
    t.decimal  "invoice_amount"
    t.integer  "appointment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patients", force: true do |t|
    t.string   "patient_name"
    t.string   "patient_email"
    t.string   "patient_address"
    t.string   "patient_phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "physician_statuses", force: true do |t|
    t.string   "phys_status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "physicians", force: true do |t|
    t.string   "physician_name"
    t.string   "physician_email"
    t.integer  "physician_status_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "time_slots", force: true do |t|
    t.string   "time_slot_string"
    t.time     "time_slot"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
