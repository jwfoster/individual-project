class AppointmentPdf < Prawn::Document
  def initialize (appointment)
    super(top_margin: 10)
    @appointment= appointment
    rule2
    text 'Individual Project Invoice', size: 24, style: :bold, align: :center
    rule
    inv_date
    pat_info
    phys_info
    app_meta_data
    diag_info
    cost
  end
  def rule
    stroke_color '000000'
    stroke do
      horizontal_rule
    end
  end
  def rule2
    stroke_color '000000'
    stroke do
      horizontal_rule
      move_down 10
    end
  end
  def inv_date
    move_down 5
    text "Date: #{Date.today.strftime('%m/%d/%Y')}", align: :right
  end
  def pat_info
    move_down 15
    text " #{@appointment.patient.patient_name}", size: 14, :style => :bold
    move_down 5
    indent 25 do
      text "#{@appointment.patient.patient_email}", size: 12, :style => :italic
    end
  end
  def phys_info
    move_down 15
    text"Physician: Dr. #{@appointment.physician.physician_name}", size: 14, style: :bold
  end
  def app_meta_data
    move_down 30
      text "Appointment Date:                 Appointment Time:", size: 12, style: :bold, align: :right
      text "#{@appointment.appointment_date.strftime('%m/%d/%Y')}                                     #{@appointment.time_slot.time_slot_string}", align: :right
  end
  def diag_info
    move_down 15
    text 'Diagnosis: ', size: 12, style: :bold
    move_down 5
    text "#{@appointment.diagnosis.diagnosis_description}", size: 12
    move_down 15
    text 'Physician Notes: ', size: 12, style: :bold
    move_down 5
    text "#{@appointment.physician_notes}", size: 12
  end
  def cost
    move_down 10
    text "Standard Appointment Fee:      $35.00", align: :right
    text "+ Diagnosis Fee:      $#{@appointment.diagnosis.diagnosis_price}0", align: :right
    rule
    move_down 10
    text "Total:      $#{@appointment.diagnosis.diagnosis_price + 35.0}0", align: :right, style: :bold
    move_down 10
    rule
  end
end
