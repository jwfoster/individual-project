class PhysicianReportPdf < Prawn::Document
  def initialize (physician)
    super(top_margin: 10)
    @physician= physician
    hrule2
    text "Dr. #{@physician.physician_name}'s Appointments Report", size: 24, style: :bold, align: :center
    hrule
    appointment_records
  end
    def hrule
      stroke_color '000000'
      stroke do
        horizontal_rule
     end
    end
    def hrule2
      stroke_color '000000'
      stroke do
        horizontal_rule
        move_down 10
      end
    end
    def appointment_records
      move_down 30
      text 'Appointment Date:               Appointment Price:', align: :center
      @physician.appointments.where({appointment_status_id: 3}).order(:appointment_date).each do |app|
        indent 182 do
        text "#{app.appointment_date}                                  $#{app.diagnosis.diagnosis_price + 35.0}0"
        end
        end
    end
  end
