json.array!(@physician_statuses) do |physician_status|
  json.extract! physician_status, :id, :physician_status
  json.url physician_status_url(physician_status, format: :json)
end
