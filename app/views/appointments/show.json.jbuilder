json.extract! @appointment, :id, :patient_id, :physician_id, :appointment_date, :time_slot_id, :appointment_notes, :diagnosis_id, :physician_notes, :appointment_status_id, :created_at, :updated_at
