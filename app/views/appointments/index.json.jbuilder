json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :patient_id, :physician_id, :appointment_date, :time_slot_id, :appointment_notes, :diagnosis_id, :physician_notes, :appointment_status_id
  json.url appointment_url(appointment, format: :json)
end
