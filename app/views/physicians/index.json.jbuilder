json.array!(@physicians) do |physician|
  json.extract! physician, :id, :physician_name, :physician_email, :physician_status_id
  json.url physician_url(physician, format: :json)
end
