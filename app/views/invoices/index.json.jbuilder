json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :invoice_date, :invoice_amount, :appointment_id
  json.url invoice_url(invoice, format: :json)
end
