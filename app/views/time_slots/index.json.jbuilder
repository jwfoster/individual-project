json.array!(@time_slots) do |time_slot|
  json.extract! time_slot, :id, :time_slot_string, :time_slot
  json.url time_slot_url(time_slot, format: :json)
end
