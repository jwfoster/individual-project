json.array!(@patients) do |patient|
  json.extract! patient, :id, :patient_name, :patient_email, :patient_address, :patient_phone
  json.url patient_url(patient, format: :json)
end
