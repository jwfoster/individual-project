json.array!(@diagnoses) do |diagnosis|
  json.extract! diagnosis, :id, :diagnosis_description, :diagnosis_price
  json.url diagnosis_url(diagnosis, format: :json)
end
