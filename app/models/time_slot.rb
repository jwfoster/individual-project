class TimeSlot < ActiveRecord::Base
  has_many :appointments
  validates_presence_of :time_slot, :time_slot_string
end
