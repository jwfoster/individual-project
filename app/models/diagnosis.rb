class Diagnosis < ActiveRecord::Base
  belongs_to :appointment
  has_many :appointments
  has_many :physicians, through: :appointments
end