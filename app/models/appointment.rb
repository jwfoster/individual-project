class Appointment < ActiveRecord::Base
  belongs_to :appointment_status
  belongs_to :physician
  belongs_to :patient
  belongs_to :time_slot
  has_one :diagnosis
  belongs_to :diagnosis

  validates_presence_of :patient_id, :physician_id, :appointment_date, :appointment_notes, :time_slot_id
  validates_uniqueness_of :time_slot_id, :scope => [:physician_id, :appointment_date], :message => '- That physician has another appointment at that time. Please select another time or physician.'

  before_update :app_complete
  before_destroy :cancel_time_check
  before_create  :date_check, :validates_weekday#, :time_check


  def cancel_time_check
       time_calc = self.time_slot.time_slot - Time.now
      if self.appointment_date == Date.today and time_calc < 8
        errors.add(:base ,'Cannot cancel an appointment within 8 hours of the scheduled appointment time')
      return false
    end
  end

  def date_check
    if self.appointment_date < Date.today
      errors.add(:appointment_date, '- Cannot schedule an appointment in the past.')
      return false
      end
  end

  #def time_check
    #date_calc= (self.appointment_date - Date.today)*24
    #time_calc= (to_time(self.time_slot.time_slot_string) - Time.now)
    #hours= date_calc + time_calc
    #if hours < 12
      ##errors.add(:time_slot , 'error- Appointment must be scheduled more than 12 hours before the appointment time')
      #return false
    #end
  #end


  def validates_weekday
    if appointment_date.saturday? == true or appointment_date.sunday? == true
      errors.add(:appointment_date, '- We are not open on the weekends')
      return false
    end
  end

  def app_complete
    if self.appointment_status_id != 3
      errors.add(:appointment_status_id, '- Please change the appointment status to "Completed"')
      return false
    end
  end
end