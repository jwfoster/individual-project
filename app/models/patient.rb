class Patient < ActiveRecord::Base
  has_many :appointments, dependent: :destroy
  belongs_to :appointment
  belongs_to :physician
  belongs_to :appointment_status

  validates_presence_of :patient_name, :patient_address, :patient_email, :patient_phone
end
