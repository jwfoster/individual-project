class Physician < ActiveRecord::Base
  belongs_to :physician_status
  has_many :appointments
  has_many :patients, through: :appointments
  has_many :time_slots, through: :appointments

  validates_presence_of :physician_email, :physician_name, :physician_status_id

end