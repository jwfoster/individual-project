class Invoice < ActiveRecord::Base
  belongs_to :appointment
  has_one :diagnosis, through: :appointment
  has_one :physician, through: :appointment
  has_one :patient, through: :appointment
  has_one :time_slot, through: :appointment
end