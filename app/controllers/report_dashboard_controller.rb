class ReportDashboardController < ApplicationController
  def index
    @physicians= Physician.all.where({physician_status_id: 1})
    respond_to do |format|
      format.html
      format.pdf do
        pdf= PhysicianReportPdf.new(@physician)
        send_data pdf.render, filename: "Physician_Appointment_Reports_#{Date.today}.pdf",
                  type: 'application/pdf',
                  disposition: 'inline'
      end
    end
  end
end
